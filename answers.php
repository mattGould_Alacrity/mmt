<html>
<head>
	<title>Monday morning HTML Quiz</title>
	
	
<style>

p {
	color: red;
	background-color: blue;
}

</style>	

</head>

<body>
	<h1>Quiz</h1>
	<p>This document is for the Thursday afternoon quiz</p>
	
	<ol>
		<li>see above</li>
		<li>HyperText Markup Language</li>
		<li>Transfer data over the internet</li>
		<li><a href='www.example.com'>click here<a></li>
		
		<h2>section 2: CSS</h2>
		
		<li>Cascading Style Sheets</li>
		<li>CSS is used to minimise the amount of styling used in a website, bycreating a single file it can be referanced and used anywhere on the website</li>
		<li>use of the style tag or refernacing a class in a linked css file</li>
		<li><p>test that this works</p></li>
		
		<h2>section 3: Classes</h2>
		
		<li>classes are considered as helpful becuase they provide structure to code and minimise the amount of code that needs to be written out because the code can be reused. classes can also provide other uses like polymorphism and inheritance</li>
		<li>classes contain variables and functions</li>
		<li>
		<?php
		class Car{
			private $manufacturer;
			private $model;
			private $color;
			private $ownerName;
			
			public function __construct($manufacturer, $model, $color, $ownerName){
				$this->manufacturer = $manufacturer;
				$this->model = $model;
				$this->color = $color;
				$this->ownerName = $ownerName;
			}
			
			public function getManufacturer(){
				return $this->manufacturer;
			}
			
			public function getModel(){
				return $this->model;
			}
			
			public function getColor(){
				return $this->color;
			} 
			
			public function getOwnerName(){
				return $this->ownerName;
			} 
		}
		?>
		</li>
		
		<li>
		<?php
		function outputToIu($manufacturer, $model, $color, $ownerName){
			echo "<p>",$ownerName," owns a ",$color," ",$manufacturer," ",$model,"</p>";
		}
		
		$car1 = new Car("Mercedes", "GLA", "Silver", "RT Games");
		$car2 = new Car("Dodge", "Viper SRT10 ACR", "White", "Matthew");
		
		outputToIu($car1->getManufacturer(), $car1->getModel(), $car1->getColor(), $car1->getOwnerName());
		outputToIu($car2->getManufacturer(), $car2->getModel(), $car2->getColor(), $car2->getOwnerName());
		?>
		</li>
		
		<li>
		<?php
		$bag= [];
		for($i = 1; $i < 100; $i++){
			if($i%2) $bag[] = $i;
		}
		?>
		</li>
		
		<li>
		<?php
		$bagTotal = 0;
		foreach($bag as $value){
			$bagTotal += $value;
		}
		
		echo "total: ",$bagTotal,"<p>";
		
		if($bagTotal > 2000){
			echo "big";
		}
		else if ($bagTotal == 2000){
			echo "meh";
		}
		else {
			echo "small";
		}
		echo "</p>";
		?>
		</li>
		
		<h2>section 3: Database Schemas</h2>
		<li>the 3 parts are: external, logical and physical. this allows use to represent the database from 3 viewpoints: the external user, the code and the actual storage</li>
		<li>a class can be represented by how the database is layed out, an instance of the class relates to the data in the database</li>
		<li>
		showroom table: id, dealership_id, url <br>
		makes table: id, name <br>
		model table: id, name <br>
		car table: id, model_id, make_id <br>
		dealership table: id, location <br>
		emloyee table: id, name <br>
		employee-dealership table: dealership_id, emloyee_id <br>
		sales table: id, employee_id, car_id <br>
		</li>
		<li>
		use sales table to work out who the best salesman is (SELECT employee_id, COUNT(employee_id) FROM sales GROUP BY employee_id) <br>
		use sales to get cars that were sold then use that data to find the best manufacterer <br>
		use sales to get cars that were sold then use that data to find the best model <br>
		doesnt store customer satisfaction with the process/product
		</li>
	</ol> 
	
</body>
</html>